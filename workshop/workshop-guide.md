# Snyk and Atlassian: Developer Security for Bitbucket 
## About this Workshop
Welcome! This workshop demonstrates how to use [Snyk Open Source](https://snyk.io/product/open-source-security-management/) within Bitbucket and Jira workflows to find and fix issues in vulnerable open source components. 

In this developer-centric workshop, you'll complete the following steps:
1. Import a Bitbucket Repo into Snyk
2. Create a Security Gate in a Bitbucket Pipelines delivery toolchain
3. Create a Jira Epic and Tickets to remediate vulnerabilities
4. Fix Vulnerabilities in the IDE
5. Unblock the Delivery Pipeline

> Note: Snyk's Jira integration, used in Part 3 of this workshop, requires a Snyk paid plan. For this workshop you can activate a Trial of Snyk's Teams Plan, which includes the Jira integration, or you can skip the optional steps that use the Jira integration.

## Your demo environment
Please ensure you complete the steps in `prerequisites.md` before continuing.

In this workshop you'll use: Bitbucket, Bitbucket Pipelines, Jira, VS Code, and Snyk.

# Part 1: Import the Atlassian Goof Repository into Snyk
To start, we'll run an initial vulnerability scan on the atlassian-goof repository to get a glimpse of the issues in the repository.

1. In Bitbucket, navigate to your import of the atlassian-goof repo. Don't have it? See how in the `prerequisites.md` document.

![Bitbucket Repo](./images/part1/bitbucket-repo.png)

2. Then, navigate to the Security tab. 

![Bitbucket Security Tab](./images/part1/bitbucket-security-tab.png)

3. Click the "Try Now" button to install the [Snyk Connect App](https://docs.snyk.io/integrations/git-repository-scm-integrations/bitbucket-cloud-integration#1st-party-integration-connect-app) into your Bitbucket workspace.

![Snyk CTA in Bitbucket](./images/part1/snyk-bitbucket-cta.png)

4. Proceed through the steps to configure the [Snyk Connect App](https://docs.snyk.io/integrations/git-repository-scm-integrations/bitbucket-cloud-integration#1st-party-integration-connect-app) and connect your Bitbucket workspace to Snyk.

![Connect to Snyk](./images/part1/snyk-bitbucket-connect.png)

> Note: If you're using a Snyk account with Bitbucket Cloud already connected, it won't ask you to create an App Password during the installation. 

4. Once authenticated, the initial scan of the Repo will begin.

![Initial Scan](./images/part1/initial-bbc-scan.png)

5. When the import completes, you'll see the Snyk scan results, grouped by the manifest file with the dependencies that introduce them. Click the `package.json` manifest file.

![Dependency Manifests](./images/part1/dependency-manifests.png)

6. Explore the Issue Cards, noticing the information Snyk provides for each vulnerability. This includes the Priority Score and information such as:
    - The issue type, and informative links to the [Snyk Intel DB](https://snyk.io/product/vulnerability-database/), CVE, and CWE
    - The dependency that introduced the vulnerability
    - Dependency version where this vulnerability was fixed

![Issue Card in Bitbucket](./images/part1/issue-card-bbc.png)

7. From any issue card, select the link to "Visit Snyk". This takes you to the Snyk UI to see the same issue cards with more information and routes for addressing the vulnerability, such as:
    - Opening a Fix Pull Request
    - Ignoring the Vulnerability

![Issue Card in Snyk](./images/part1/issue-card-snyk.png)

8. Feel free to explore the other Tabs in the Project Page, including the Fixes and Dependencies tab. 

![Issues, Fixes, and Dependencies Tabs](./images/part1/oss-project-tabs.png)

## You completed Part 1! 

Part 1 guided us through onboarding a Repo into Snyk and running the initial scan. In Part 2 we'll test for vulnerabilities within a Bitbucket Pipeline, and use Snyk to "stop the bleeding" - preventing new vulnerabilities from entering our Production environment.

# Part 2: Stop the bleeding in the Bitbucket Pipeline!
A core part of shifting security left is catching these issues early. This can be paired with safeguards such as security checks in the toolchain to prevent vulnerabilities from entering Production. 

In this section, you'll use Bitbucket Pipelines to simulate the deployment of this application to Production, using the Snyk Pipe to create a quality gate to catch vulnerable components before the application is deployed. Let's begin!

1. Return to the Snyk UI and ensure you're in the same Snyk Organization you used in Part 1 of the workshop.

![Workshop Organization](./images/part2/workshop-org.png)

2. Navigate to [Snyk Account Settings](https://app.snyk.io/account) and copy your API Token to your clipboard.

![API Token](./images/part2/snyk-api-token.png)

> Note: If you're using a Snyk Paid Plan, you can opt to use a [Service Account](https://docs.snyk.io/integrations/managing-integrations/service-accounts) instead. 

3. Return to Bitbucket, navigate to "Pipelines" in the Repo sidebar, and enable Bitbucket Pipelines.

![Enable Pipelines](./images/part2/enable-pipelines.png)

> Note: After you enable Bitbucket Pipelines, the initial run will start and fail since we have not provided the Snyk API Token.

4. Pass the Snyk API Token to Bitbucket Pipelines by opening the Repository Settings and saving the Snyk API Token as a Repository Variable called SNYK_TOKEN.

![Snyk Token Repo Variable](./images/part2/snyk-token-repo-var.png)

The token can now be accessed by Bitbucket Pipelines.

5. With the Token saved, return to Pipelines and select "Run Pipeline". 

![Run Pipeline](./images/part2/run-pipeline.png)

In the "Run Pipeline" dialog, select the `main` branch and the `default` pipeline before clicking "Run". 

![Configure Pipeline Run](./images/part2/config-run-pipeline.png)

6. While it runs, return to the Source Tab and select the `bitbucket-pipelines.yml` file to see what it's going to do. 

![Find Pipelines YML](./images/part2/view-pipeline-yml.png)

This pipeline:
- Resolves the Open Source Dependencies (npm install)
- Tests the Dependencies with Snyk Open Source
- Tests, Packages, and Deploys the Application (mock steps)

Notice that although the failure conditions are set, the Snyk Pipe is set to not break the build in case vulnerabilities are found.

![Snyk Pipe](./images/part2/snyk-pipe-no-break.png)

6. Because Snyk is set to not break the build, the pipeline completes and a snapshot of the vulnerabilities in the application are uploaded to Snyk as a new Project. Imagine these vulnerabilities in Production! 

![Snyk Projects](./images/part2/snyk-both-projects.png)

7. To stop the bleeding, edit the `bitbucket-pipelines.yml` file using the Bitbucket Web Editor. Change the value of `DONT_BREAK_BUILD` from true to false then commit the change.

![Breaking the Build](./images/part2/snyk-pipe-break-build.png)

8. Once the Pipeline re-runs, you'll have a failed run. Snyk failed the build before the test, package, and deploy stages. Failing the build early allows you to save build minutes on your pipeline!

![Broken Build](./images/part2/snyk-failed-pipeline.png)

> Check out [Advanced Failing of Builds in Snyk CLI](https://docs.snyk.io/snyk-cli/test-for-vulnerabilities/advanced-failing-of-builds-in-snyk-cli) to learn other options for breaking the build! 

## You completed Part 2! 

Part 2 helped you visualize how Snyk is implemented into a delivery toolchain. Using the Snyk Pipe, we stopped the bleeding to prevent vulnerabilities from entering Production, but now have a broken delivery pipeline! In Part 3 we review the issues blocking the deployment and create work items for developers to kick off remediation.

# Part 3: Creating Work Items for Developers
In this section, we'll assume the role of a development team manager and create Jira issues for developers to unblock the delivery pipeline by addressing the vulnerabilities that triggered the Snyk quality gate. 

1. First, review the failure conditions for our quality gate by reviewing the `bitbucket-pipelines.yml` file. 

![Fail Conditions](./images/part3/snyk-pipe-fail-conditions.png)

The `SEVERITY_THRESHOLD` parameter tells Snyk to fail the build when Critical severity vulnerabilities are found. 

2. You can also review the vulnerabilities that failed the build by viewing the Code Insights report from the Pipeline run.

![Code Insights](./images/part3/code-insights-link.png)
![Code Insights Report](./images/part3/code-insights-report.png)

Knowing what exact issues failed the pipeline helps focus developers on the vulnerabilities that are blocking the pipeline as the most urgent to fix.

3. Let's prepare to create work items for developers by creating a Jira Epic to track the work to unblock the pipeline. Log in to Jira, and select to create an Issue.

![Create Button](./images/part3/jira-create.png)

 Provide a summary of the work to be performed to complete this epic. In this case, remediating the critical severity vulnerabilities with fixes available.

![Create the Epic](./images/part3/jira-create-epic.png)

4. OPTIONAL STEP: Snyk's Jira integration can create work items under the Epic we created. Follow the steps to [Set up the Snyk Jira Integration](https://docs.snyk.io/integrations/notifications-ticketing-system-integrations/jira).

You'll need to [create an Atlassian API Token](https://id.atlassian.com/manage-profile/security/api-tokens) to configure Jira.

![Setup Jira Integration](./images/part3/setup-jira-integration.png)

> Note: Snyk's Jira integration requires a paid Snyk plan, so this is an optional step. Feel free to skip this step and watch the facilitator go through it, or activate a Trial of Snyk's Teams Plan to try it yourself.

4. In Snyk, navigate to either `package.json` Project imported from Bitbucket Cloud, and filter the list of issues to the ones blocking our delivery pipeline with the following filters:
    - Vulnerabilities
    - Critical Severity

![Imported Projects](./images/part3/imported-projects.png)

![Filtered Issues](./images/part3/filtered-snyk-issues.png)

We see there are only three issues to worry about! 

5. OPTIONAL STEP: From Snyk, create Jira tickets for the issues and link them to the Epic we created. Note the Epic Key in the screenshot will be different than yours. 

![Create Jira Issues](./images/part3/create-jira-issues.png)

![Issue Creation Details](./images/part3/issue-creation-details.png)

> Note: Snyk's Jira integration requires a Snyk paid plan, so this is an optional step. Feel free to skip this step and watch the facilitator go through it, or activate a Trial of Snyk's Teams Plan to try it yourself.

6. In Jira, you can now see the issues under the Epic.

![Issues in Jira Epic](./images/part3/issues-in-epic.png)

## You completed Part 3! 

Part 3 demonstrated how work flows from those managing delivery pipelines to developers that perform the fix to unblock the pipeline. In some cases, the same developers that perform the fix also own the delivery pipeline! In Part 4 we'll dive into the issue triggering our quality gate and unblock the deployment.

# Part 4: Take Action to Fix Vulnerabilities
In this Part we'll explore how developers can fix the vulnerabilities blocking the pipeline using the Snyk and Atlassian Plugins for VS Code.

> Note: Ensure you completed the steps in `prerequisites.md` to set up the Snyk and Atlassian plugins for VS Code.

1. From Bitbucket, Clone the Repo to VS Code using the Clone button at the top of the page and selecting "Clone in VS Code".

![Clone Repo](./images/part4/clone-repo.png)
![Clone in VS Code](./images/part4/clone-vs-code.png) 

2. In VS Code, use the Atlassian extension to review the Jira Epic created in Module 3. If you created the Issues from Snyk, review the issues by selecting them.

![Jira Issues in VS Code](./images/part4/atlassian-vscode-jira.png)


2. Let' start tackling this Epic. Open the Epic, then from the issue details, click "Start Work".

![Issue Details in VS Code](./images/part4/jira-vscode-issue-view.png)

3. Fill out the Start Work page:
    - Transition the Jira issue to In Progress
    - Set "main" as the source branch
    - Create a new branch for the code changes

![Start work Menu](./images/part4/jira-vscode-start-work.png)

We can see information about the current active branch and linked Jira issues in the bottom bar of the IDE.

![Active Branch & Jira Issue](./images/part4/atlassian-active-items.png)

4. Use the Snyk Plugin to kick off a scan, then review the Snyk results. Notice the Critical Severity vulnerabilities under "Open Source Security".

![Critical Issues](./images/part4/vscode-critical-issues-pre-fix.png)

3. Clicking on an issue displays an issue card with the same information we saw in the Snyk and Bitbucket UIs, including the "Fixed In" version of the dependency. 

![Open Source Issue Card](./images/part4/issue-card-IDE.png)

> Note: Open Source vulnerabilities are disclosed daily, so recommendations may differ as the Snyk database is constantly updated. This example shows upgrade recommendations as of the day of writing.

4. To apply the secure version of the dependency without the blocking issue, navigate to the `package.json` file and replace the version of `adm-zip` with the non-vulnerable one.

![Updating Dependency](./images/part4/goof-oss-manifest-fix.png)

5. When ready, save the changes. Now re-generate your lockfile by running the following command:

```sh
npm install --package-lock
```

You can see the changes made to the lockfile in the VSCode Diff View.
![Lockfile Diff](./images/part4/vscode-diff-view.png)

6. Now, re-scan with the Snyk Plugin to verify the issue is gone. 

![Leftover Critical Issues](images/part4/vscode-critical-issues-post-fix.png)

7. In the VS Code SCM view, stage, commit, and sync the changes.

![Stage Changes](./images/part4/stage-changes.png)

![Commit changes](./images/part4/commit-changes.png)

![Sync changes](./images/part4/sync-changes.png)

8. Our `bitbucket-pipelines.yml` CI workflow resolves dependencies and runs tests when changes are made to branches other than `main`. Continuous Testing after upgrading dependencies is important to ensure no breaking changes are introduced before merging to `main`.

![Bitbucket CI](./images/part4/bitbucket-pipelines-ci.png)

9. Repeat steps 3-8 for the other Critical Vulnerabilities. Eventually, you'll land at two vulnerabilities you can't fix introduced by `tap@11.1.5`'s transitive dependencies `lodash` and `hbs`. 

There is no minor upgrade available for `tap@11`. In this case we'll find a new major version to upgrade to. [Snyk Advisor](https://snyk.io/advisor/npm-package/tap) can help us find explore newer versions.

![Snyk Advisor for tap](./images/part4/advisor-tap.png)

For this example we'll upgrade tap to `tap@12.7.0`. Repeat steps 5-8 to ensure no breaking changes were introduced.

> Note: Upgrading dependencies to newer versions may introduce breaking changes. In some cases, you may want to [ignore the vulnerabilities](https://docs.snyk.io/snyk-cli/fix-vulnerabilities-from-the-cli/ignore-vulnerabilities-using-snyk-cli) at your discretion until a fix is available.

## You completed Part 4!
In Part 4 you saw how developers can work with the Snyk and Atlassian Plugins in VS Code to explore Jira tickets, update vulnerable dependencies, and track the CI runs ensuring no breaking changes are introduced. 


# Part 5: Merge the Fixes to Unblock the Pipeline
Finally, let's unblock our pipeline by opening a Pull Request from the `fix-dependencies` branch we worked on in Part 4 into the `main` branch.  

1. Back in Bitbucket, select the Pull Request Menu and create a Pull Request from `fix-dependencies` into `main`.

![Create PR](images/part5/bitbucket-pr-create.png)
![PR Dialog](images/part5/bitbucket-pr-details.png)

> Pro Tip: Mentioning the Jira Issue Keys in the Pull Request Title associates them with the Pull Request.

2. In the Pull Request, notice Snyk runs two incremental analysis against the `main` branch monitored by the Bitbucket Integration. 

![PR Checks](./images/part5/snyk-pr-tests.png)

> The pass/fail conditions of the PR Tests can be adjusted in the [Bitbucket Integration Settings](https://docs.snyk.io/integrations/git-repository-scm-integrations/bitbucket-cloud-integration#pull-request-tests) in the Snyk UI.

3. Approve and Merge the Pull Request. You can also delete the `fix-vulnerabilities` branch and close the Jira epic created earlier.

![Merge PR](images/part5/merge-pr.png)

4. Once the PR is merged, return to Bitbucket Pipelines to see the pipeline successfully execute! You unblocked the deployment!

![Pipeline Success](./images/part5/pipeline-success.png)

If your build fails, clear the Bitbucket Pipelines cache to remove stale dependencies then re-run the Pipeline.

![Clear Cache](./images/part5/clear-cache.png)

In Snyk, the Critical Severity issues are gone from both the Bitbucket Project and the Bitbucket Pipeline import!

![No Critical Vulns](./images/part5/snyk-ui-fixed.png)

In Bitbucket, the Critical severity issues are gone as well.

![No Criticals in Bitbucket](./images/part5/bbc-snyk-app-fixed.png)

## You completed Part 5!
Congratulations! Your delivery pipeline is now unblocked and your Production application a lot more secure.

# Bonus: Want to Automate Part 4?

In Part 4 you played the role of a developer to fix vulnerabilities identified by Snyk in the IDE, but what if you wanted to do it different?

Check out [Snyk's Automatic Fix Pull Requests](https://docs.snyk.io/integrations/git-repository-scm-integrations/bitbucket-cloud-integration#projects-monitoring-and-automatic-fix-pull-requests) to see how! 

# Conclusion

You reached the end of this workshop! This is one example of how Snyk guides developers through remediating vulnerabilities. There is much more we didn’t show, including Snyk's other products.

If you’re interested in Snyk's other capabilities, let us know! Your feedback helps us direct what workshops to create for events like this one.

Thanks for attending. <3