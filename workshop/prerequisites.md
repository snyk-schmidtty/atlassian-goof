# Pre-requisites for the Team 22 Hands-On Session
## Please complete these prior to the workshop! 

In this workshop, you will use of the following tools. Create free accounts for:
- Snyk
- Bitbucket
- Jira

You'll also need access to a few developer tools.
- VS Code, or any IDE of your choosing (we use VS Code in the workshop)
- Atlassian Extension for VS Code
- Snyk Extension for VS Code

Snyk's Jira integration, used in this workshop, is only available in Snyk's paid Snyk plans. For this workshop you can activate a Trial of Snyk's Teams Plan, which includes the Jira integration, or you can skip the Jira steps in Module 4.

### Import the Repo with the Sample Code into Bitbucket
Everything you need for this workshop is in the [atlassian-goof](https://bitbucket.org/tomas-snyk/atlassian-goof/src/main/) repo. Follow the steps to import it to your Bitbucket workspace.

1. Sign in to Bitbucket.
2. Clone the Repo using the [Import Repository](https://support.atlassian.com/bitbucket-cloud/docs/import-a-repository/) functionality.

### Create a Snyk Account and Install the Snyk Plugin for VS Code
#TODO: ADD UTM
1. [Create a Snyk Account](UTM) for yourself, or use your existing Snyk account. 

#TODO: ADD UTM

2. Visit the documentation for the [Snyk Extension for VS Code](https://docs.snyk.io/integrations/ide-tools/visual-studio-code-extension-for-snyk-code), then install and authenticate it on your system. 

### Install and Authenticate the Atlassian for VS Code Extension
If you're using VS Code, set up the Atlassian IDE Extension. 

1. Follow the steps to [Get Started with VS Code](https://support.atlassian.com/bitbucket-cloud/docs/get-started-with-vs-code/).